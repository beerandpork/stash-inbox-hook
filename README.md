# What?

A simple repository hook for displaying pull requests that require your attention.

    These pull requests are ready to merge:

        Map reduce
        https://stash.dev/projects/STASH/repos/log-parser/pull-requests/1

        RestfulTableEventNamespacing
        https://stash.dev/projects/TEST/repos/au-test/pull-requests/1

    These pull requests require your approval:

        CSS improvements
        https://stash.dev/projects/QA/repos/pr-test/pull-requests/6

# Why?

    I must not context switch.
    Context switching is the productivity killer.
    Context switching is the little-death that brings total distraction.

Distracting a developer whilst they're in the middle of a task is [bad juju](http://heeris.id.au/2013/this-is-why-you-shouldnt-interrupt-a-programmer). **But**, pull requests need to be regularly reviewed and actioned in order to ship code to customers.

This add-on strikes a balance. Developers are notified of outstanding pull requests when they push code to Stash. After a push is often a logical point to take a break from development and switch to a different task, like actioning pull requests.
