package com.atlassian.stash.inboxhook;

import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.hook.HookResponse;
import com.atlassian.stash.hook.PostReceiveHook;
import com.atlassian.stash.inboxhook.settings.InboxSettings;
import com.atlassian.stash.inboxhook.settings.InboxSettingsFactory;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.pull.PullRequestParticipant;
import com.atlassian.stash.pull.PullRequestRole;
import com.atlassian.stash.pull.PullRequestService;
import com.atlassian.stash.pull.PullRequestState;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.scm.pull.MergeRequestCheckService;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;
import com.atlassian.stash.util.Page;
import com.atlassian.stash.util.PageRequest;
import com.atlassian.stash.util.PageUtils;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

import static com.google.common.collect.Iterables.limit;

/**
 * A post-receive hook for displaying open pull requests that require your attention.
 */
public class InboxHook implements PostReceiveHook {

    private static final Logger log = LoggerFactory.getLogger(InboxHook.class);

    private static final int MAX_ITEMS = 10;
    private static final PageRequest PAGE_REQUEST = PageUtils.newRequest(0, MAX_ITEMS);
    private Page<PullRequest> EMPTY_PAGE = PageUtils.createEmptyPage(PAGE_REQUEST);

    private static enum ItemState {
        readyToMerge,
        requireApproval
    }

    private final PullRequestService pullRequestService;
    private final MergeRequestCheckService mergeRequestCheckService;
    private final StashAuthenticationContext authenticationContext;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final InboxSettingsFactory inboxSettingsFactory;

    public InboxHook(PullRequestService pullRequestService, MergeRequestCheckService mergeRequestCheckService,
                     StashAuthenticationContext authenticationContext, SoyTemplateRenderer soyTemplateRenderer,
                     InboxSettingsFactory inboxSettingsFactory) {
        this.pullRequestService = pullRequestService;
        this.mergeRequestCheckService = mergeRequestCheckService;
        this.authenticationContext = authenticationContext;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.inboxSettingsFactory = inboxSettingsFactory;
    }

    @Override
    public void onReceive(@Nonnull Repository repository, @Nonnull Collection<RefChange> refChanges,
                          @Nonnull HookResponse response) {
        StashUser currentUser = authenticationContext.getCurrentUser();

        // this hook does not support anonymous pushes (but hey, neither does Stash)
        if (currentUser == null) {
            return;
        }

        InboxSettings inboxSettings = inboxSettingsFactory.createSettingsFor(currentUser);

        Multimap<ItemState, PullRequest> inbox = HashMultimap.create();

        Page<PullRequest> requireApproval = inboxSettings.isShowRequireApproval() ?
                pullRequestService.findByParticipant(
                    currentUser,
                    PullRequestRole.REVIEWER,
                    false,
                    PullRequestState.OPEN,
                    null,
                    PAGE_REQUEST
                ) :
                EMPTY_PAGE;

        Page<PullRequest> authoredAndOpen = inboxSettings.isShowReadyToMerge() ?
                pullRequestService.findByParticipant(
                    currentUser,
                    PullRequestRole.AUTHOR,
                    null,
                    PullRequestState.OPEN,
                    null,
                    PAGE_REQUEST
                ) :
                EMPTY_PAGE;

        // Display up to MAX_ITEMS pull requests, balanced between the two categories
        int authoredToShow = Math.max(MAX_ITEMS / 2, MAX_ITEMS - authoredAndOpen.getSize());
        Iterable <PullRequest> limitedAuthored = limit(requireApproval.getValues(), authoredToShow);

        inbox.putAll(ItemState.requireApproval, limitedAuthored);

        Iterator<PullRequest> iterator = authoredAndOpen.getValues().iterator();
        while (inbox.size() < MAX_ITEMS && iterator.hasNext()) {
            PullRequest pullRequest = iterator.next();
            // TODO also check build status
            if (isApproved(pullRequest) && mergeRequestCheckService.check(pullRequest).isEmpty()) {
                inbox.put(ItemState.readyToMerge, pullRequest);
            }
        }

        if (!inbox.isEmpty()) {
            Map<String, Object> context = Maps.newHashMap();
            for (Map.Entry<ItemState, Collection<PullRequest>> entry : inbox.asMap().entrySet()) {
                context.put(entry.getKey().name(), entry.getValue());
            }

            // ensure keys are present for empty categories
            for (ItemState state : ItemState.values()) {
                if (!context.containsKey(state.name())) {
                    context.put(state.name(), Collections.emptyList());
                }
            }

            render(response.out(), "inbox.hook.inbox", context);
        }
    }

    private static boolean isApproved(PullRequest pullRequest) {
        boolean hasReviewer = false;
        for (PullRequestParticipant participant : pullRequest.getParticipants()) {
            if (PullRequestRole.REVIEWER.equals(participant.getRole())) {
                if (participant.isApproved()) {
                    return true;
                }
                hasReviewer = true;
            }
        }
        // If there aren't any REVIEWERs assigned, return approved = true.
        return !hasReviewer;
    }

    private void render(Appendable appendable, String template, Map<String, Object> context) {
        try {
            try {
                soyTemplateRenderer.render(appendable, "com.atlassian.stash.plugin.stash-inbox-hook:soy-templates",
                        template, context);
            } catch (SoyException e) {
                appendable.append("Failed to render " + template + ": " + e.getMessage());
                log.error("Failed to render " + template, e);
            }
        } catch (IOException e) {
            log.error("Failed to write to hook response.", e);
        }
    }
}
