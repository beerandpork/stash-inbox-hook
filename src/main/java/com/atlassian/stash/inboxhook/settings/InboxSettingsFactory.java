package com.atlassian.stash.inboxhook.settings;

import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.stash.user.StashUser;

/**
 * Produces {@link InboxSettings} for the context user.
 */
public class InboxSettingsFactory {

    public final PluginSettingsFactory pluginSettingsFactory;

    public InboxSettingsFactory(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    public InboxSettings createSettingsFor(StashUser currentUser) {
        return new InboxSettings(pluginSettingsFactory.createGlobalSettings(), currentUser.getName());
    }

}
