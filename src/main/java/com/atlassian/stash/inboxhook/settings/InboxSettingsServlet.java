package com.atlassian.stash.inboxhook.settings;

import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.user.StashAuthenticationContext;
import com.atlassian.stash.user.StashUser;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * A servlet for providing inbox hook settings on the user account page.
 */
public class InboxSettingsServlet extends HttpServlet {

    private static final String WEB_RESOURCE_CONTEXT = "user.account.inbox.settings";
    private static final String READY_TO_MERGE = "readyToMerge";
    private static final String REQUIRE_APPROVAL = "requireApproval";

    private static final Logger log = LoggerFactory.getLogger(InboxSettingsServlet.class);

    private final NavBuilder navBuilder;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final InboxSettingsFactory inboxSettingsFactory;
    private final StashAuthenticationContext authenticationContext;
    private final WebResourceManager webResourceManager;

    public InboxSettingsServlet(NavBuilder navBuilder, SoyTemplateRenderer soyTemplateRenderer,
                                InboxSettingsFactory inboxSettingsFactory,
                                StashAuthenticationContext authenticationContext,
                                WebResourceManager webResourceManager) {
        this.navBuilder = navBuilder;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.inboxSettingsFactory = inboxSettingsFactory;
        this.authenticationContext = authenticationContext;
        this.webResourceManager = webResourceManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        InboxSettings inboxSettings = getInboxSettingsForUser(resp);
        if (inboxSettings == null) {
            return;
        }

        Map<String, Object> context = Maps.newHashMap();
        context.put(READY_TO_MERGE, inboxSettings.isShowReadyToMerge());
        context.put(REQUIRE_APPROVAL, inboxSettings.isShowRequireApproval());
        context.put("saved", req.getParameter("saved"));

        webResourceManager.requireResourcesForContext(WEB_RESOURCE_CONTEXT);
        resp.setContentType("text/html");
        render(resp.getWriter(), "inbox.settings.user", context);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        InboxSettings inboxSettings = getInboxSettingsForUser(resp);
        if (inboxSettings == null) {
            return;
        }

        inboxSettings.setShowReadyToMerge(Boolean.parseBoolean(req.getParameter(READY_TO_MERGE)));
        inboxSettings.setShowRequireApproval(Boolean.parseBoolean(req.getParameter(REQUIRE_APPROVAL)));

        resp.sendRedirect(navBuilder.pluginServlets().path("inbox-hook", "settings")
                                                     .withParam("saved", "true").buildRelative());
    }

    private InboxSettings getInboxSettingsForUser(HttpServletResponse resp) throws IOException {
        StashUser stashUser = authenticationContext.getCurrentUser();
        if (stashUser == null) {
            resp.sendError(401, "You must be authenticated to access this resource.");
            return null;
        }
        return inboxSettingsFactory.createSettingsFor(stashUser);
    }

    private void render(Appendable appendable, String template, Map<String, Object> context) throws IOException {
        try {
            soyTemplateRenderer.render(appendable, "com.atlassian.stash.plugin.stash-inbox-hook:soy-templates",
                    template, context);
        } catch (SoyException e) {
            appendable.append("Failed to render " + template + ": " + e.getMessage());
            log.error("Failed to render " + template, e);
        }
    }

}
